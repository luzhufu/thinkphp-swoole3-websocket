<?php


// 事件定义文件
return [
    'bind' => [
    ],

    'listen' => [
        'AppInit' => [],
        'HttpRun' => [],
        'HttpEnd' => [],
        'LogLevel' => [],
        'LogWrite' => [],
        'swoole.task' => ['\app\listener\SwooleTask'],
        'swoole.finish' => ['\app\listener\SwooleTaskFinish'],
        //init中无法使用swoole_timer_tick等函数
        'swoole.init' => ['\app\listener\SwooleBoot'],
    ],

    'subscribe' => [],
];
