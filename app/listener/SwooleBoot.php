<?php


namespace app\listener;


use Swoole\Exception;
use Swoole\Server;

class SwooleBoot
{
    public function handle(Server $server)
    {
        //对配置文件内的所有class与methods检查
        foreach (config('task.alias') as $alias => $conf) {
            if (!class_exists($conf['class'])) {
                throw new Exception('not found class:' . $conf['class'] . ' ,请检查配置文件!');
            }
            $reflect = new \ReflectionClass($conf['class']);
            foreach ($conf['methods'] as $method) {
                if ($reflect->hasMethod($method) && $reflect->getMethod($method)->isPublic()) {
                    continue;
                } else {
                    throw new Exception('class ' . $conf['class'] . ' method:' . $method . ' attribute err,请检查配置文件/逻辑方法!');
                }
            }
            if ($conf['finish']['enable']) {
                $reflect = new \ReflectionClass($conf['finish']['class']);
                foreach ($conf['finish']['methods'] as $method) {
                    if ($reflect->hasMethod($method) && $reflect->getMethod($method)->isPublic()) {
                        continue;
                    } else {
                        throw new Exception('class ' . $conf['finish']['class'] . ' method:' . $method . ' attribute err,请检查配置文件/逻辑方法!');
                    }
                }
            }
        }
        foreach (config('websocket.alias') as $alias => $conf) {
            if (!class_exists($conf['class'])) {
                throw new Exception('not found class:' . $conf['class'] . ' ,请检查配置文件!');
            }
            $reflect = new \ReflectionClass($conf['class']);
            foreach ($conf['methods'] as $method) {
                if ($reflect->hasMethod($method) && $reflect->getMethod($method)->isPublic()) {
                    continue;
                } else {
                    throw new Exception('class ' . $conf['class'] . ' method:' . $method . ' attribute err,请检查配置文件/逻辑方法!');
                }
            }
        }

        return;
    }
}