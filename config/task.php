<?php
//swoole.task配置文件
return [
    'key' => 'cmd',
    'alias' => [
        'lottery' => [
            'class' => \app\service\task\Lottery::class,
            'methods' => [
                'participation',
            ],
            'finish' => [
                'enable' => true,
                'class' => \app\service\task\Lottery::class,
                'methods' => [
                    'finish',
                ],
            ],
        ]
    ],
];