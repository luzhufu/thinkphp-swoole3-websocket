<?php

//swoole.websocket行为配置文件
return [
    'key' => 'cmd',
    'alias' => [
        'sys' => [
            'class' => \app\service\websocket\Sys::class,
            'methods' => [
                'ping',
                'syncMsg'
            ],
        ],
        //群组
        //cmd:group默认会执行methods下所有方法,指定其中一个/多个方法是cmd:group.add/cmd:group.sponsor.add
        'group' => [
            'class' => \app\service\websocket\Group::class,
            'methods' => [
                'add',
                'sponsor',
                'quit',
            ],
        ]
    ],
];